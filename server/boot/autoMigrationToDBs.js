// var async = require('async');
module.exports = function(app) {
  // async.parallel({
  //   lojas: async.apply(autoMigrateDB('mariaDB', 'loja')),
  // }, function(err, results) {
  //   if (err) throw err;
  // });
  // });


  autoMigrateDB('mariaDB', 'loja');
  autoMigrateDB('mongoDB', 'produto');



  function autoMigrateDB(datasource, table) {
    let db = (datasource == 'mariaDB') ? app.dataSources.mariaDB : app.dataSources.mongoDB;
    db.autoupdate(table, function(err) {
      if (err) throw err;
    });
  }
};

